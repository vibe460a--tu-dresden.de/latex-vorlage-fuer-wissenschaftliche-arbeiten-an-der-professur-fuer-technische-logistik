\color{black}
In diesem Kapitel fassen Sie Ihre Arbeit zusammen, beantworten - soweit möglich - Ihre Forschungsfragen, weisen auf Grenzen und Einschränkungen Ihrer Erkenntnisse hin und geben einen Ausblick auf weitergehende Forschungsfragen, die sich aus Ihrer Arbeit ergeben.

\color{gray}
Durch die Bearbeitung der aus der Aufgabenstellung abgeleiteten Forschungsfragen konnten in Hinblick auf das Fallbeispiel einige Erkenntnisse gewonnen werden. So ist die Festlegung von Prognosehorizont und Prognoseintervall für die strategische Planung für das Unternehmen aus dem Fallbeispiel in \acs{kap} \ref{nutzung} beschrieben. In diesem Kapitel wird gleichermaßen begründet, weshalb die Absatzprognose, als ursprünglicher Gegenstand dieser Arbeit, hier durch eine Prognose der Marge zu ersetzen ist, um einen Mehrwert bezüglich der strategischen Planung zu erhalten. Weiterhin werden geeignete Prognoseverfahren vorgestellt, und die Erkennung und Bewertung signifikanter Einflussgrößen in \acs{abschn} \ref{ordnerstruktur} diskutiert. Abschließend sollen nunmehr die Erkenntnisse bezüglich der Forschungsfragen diskutiert und ein Ausblick gegeben werden, um die Ergebnisse einzuordnen.

\subsubsection{Können \acs{ML}-Methoden in Hinblick auf das Fallbeispiel eine bessere Prognosegüte als konventionelle Prognosemethoden gewährleisten?}

Für die Prognose in der hier festgelegten Form hat sich gezeigt, dass das entwickelte multivariate \ac{ML}-Modell im Vergleich mit konventionellen Verfahren und auch dem univariaten \acs{ML}-Ansatz eine bessere Performance erzielt. Das herangezogenen Fehlermaß \acs{smape} liegt mit 31.55 \% (\acs{vgl} \acs{tab.} \ref{tab_results}) um 8 \% und 5 \% unter denen der besten konventionellen Methode, die hier auf naive Weise den letzten Beobachtungswert desselben Saisonzeitpunktes (\acs{vgl} \acs{abschn} \ref{vergleichsmodelle}) als Prognosewert ausgibt. Dabei ist zu berücksichtigen, dass dieses Ergebnis lediglich für den vorliegenden spezifischen Fall und die durchgeführte \ac{ms}-Prognose mit einem Prognoseintervall von einem Monat und einem Prognosehorizont von drei Monaten repräsentativ ist. 

Als Einschränkung ist zu nennen, dass einige der für den Vergleich herangezogenen Modelle ebenfalls durch eine Hyperparameteroptimierung verbessert werden können. Diese Optimierung erfordert jedoch zusätzliche Expertise\footnote{Dass auch konventionelle statistische Prognosemethoden Potenzial für Optimierung bieten, verdeutlicht beispielsweise die \textit{M-Competition} von \textcite{Makridakis2000}.} sowie einen gewissen Aufwand in der Implementierung und wird daher im Rahmen dieser Arbeit nicht berücksichtigt. Es ist anzunehmen, dass im Zuge weiterer Untersuchungen auf diesem Wege die Performance des \acs{arima} und Holt-Winters Modells gesteigert werden könnte, wodurch der Vergleich mit dem multivariaten \ac{ML}-Modell möglicherweise anders ausfiele.

\subsubsection{Welche Einflussfaktoren lassen sich zu welchem Grad anhand einer \ac{ML}-Prognose im Fallbeispiel als relevant identifizieren?}

Die \ac{FS} (\acs{vgl} \acs{abschn} \ref{ordnerstruktur} und \ref{lokal_schreiben}) aus 276 Features (\acs{vgl} \acs{abschn} \ref{lokal_schreiben})  hat ergeben, dass der Output des \ac{rf}-Algorithmus und damit der des mulitvariaten \ac{ML}-Modells zu 80 \% durch die folgenden fünf Features \acs{bzw} Eingangsgrößen bestimmt wird:

\begin{itemize}
    \item dem Transportvolumen des Vormonats zu 20 \%,
    \item der Anzahl der betriebsbereiten Lkw zu 17 \%,
    \item dem Containerumschlag des Vormonats im Hafen Shanghai zu 14 \%,
    \item dem Kurs eines \acs{etf} auf den globalen Aktienindex von \textit{MSCI} zu 13 \%,
    \item der Gesamtstaulänge auf deutschen Autobahnen zu 10 \%,
    \item dem Rohölpreis zu 6 \%.
\end{itemize}

Der Einfluss der Lkw-Anzahl kann als Anzeichen gesehen werden, dass die Zielgröße trotz der vorgenommenen Datenintegration (\acs{vgl} \acs{abschn} \ref{sec:ergebnisse}) weitere unternehmensinterne Abhängigkeiten aufweist. Wie in \acs{abschn} \ref{lokal_schreiben} herausgearbeitet, ist dies jedoch ungünstig, falls die Prognose als Entscheidungshilfe herangezogen werden soll. Diesbezüglich könnten weiterführende Untersuchungen von Vorteil sein.

Davon abgesehen sind die angeführten Einflussgrößen plausibel, da im Rahmen der Ist-Analyse in Hinblick auf potenziell relevante Prädiktoren gewonnene Erkenntnisse (\acs{vgl} \acs{abschn} \ref{lokal_schreiben}) sich in den Ergebnissen widerspiegeln, wie beispielsweise der Einfluss aus Shanghai als häaufigster Ursprungshafen der transportierten Container. Wären die von den Expert:innen genannten Größen irrelevant, ist anzunehmen, dass der Algorithmus mit univariaten Features aus der Zeitreihe der Zielgröße bessere Ergebnisse erzielen würde. Zum einen tauchen diese jedoch nicht in der Übersicht der relevanten Features auf und zum anderen schneidet das rein univariate \ac{ML}-Modell in Hinblick auf die Performance deutlich schlechter als das multivariate Modell ab (\acs{vgl} \acs{tab.} \ref{tab_results}).

Als Einschränkung ist zu erwähnen, dass die hier genannten Einflussfaktoren lediglich anhand von Regression \acs{bzw} dem verwendeten \ac{ML}-Modell identifiziert worden sind und daher weder von Kausalität in Richtung der Zielgröße noch von einer absoluten Lösung auszugehen ist.

\subsubsection{Ausblick}

Die Bearbeitung der Aufgabenstellung hat aufgezeigt, dass sich das Erarbeiten eines geeigneten \ac{ML}-Prognoseverfahrens im Unternehmensumfeld durchaus als Herausforderung erweisen kann.

Dies liegt zum einen darin begründet, dass sich eine exakte Definition der Prognoseaufgabe als schwierig erweist, wenn bisher kein quantitatives Prognoseverfahren angewandt und dokumentiert wird, sodass ein initialer Prognoseprozess angestoßen werden muss. Dieser erfordert Untersuchungen und Überlegungen in Hinblick auf alle Merkmale, die eine Prognose vollständig definieren (\acs{vgl} \acs{abschn} \ref{fallbeispiel}). Darüber hinaus sollten Zusammenhänge vollständig verstanden werden.

Zum anderen erweist sich die interne sowie externe Datenerhebung, -bereinigung und -integration als äußerst aufwändig, falls Daten nicht zentral und standardisiert gehalten werden. In diesem Zuge könnten geeignete Schnittstellen zu einem geringeren Aufwand beitragen, was mehr Kapazität für die Datenanalyse \acs{bzw} Implementierung von Modellen schaffen und daher vermutlich zu besseren Ergebnissen führen würde. Außerdem könnten Modelle stets anhand aktueller Daten gebildet, evaluiert und validiert werden.

Davon abgesehen kann es in der betrieblichen Praxis grundsätzlich von Vorteil sein, Prognosemodelle nicht in sich geschlossen, sondern innerhalb eines sogenannten \textit{Expert:innensystem} zu implementieren, um die Zugänglichkeit für Anwendende zu vereinfachen, eine Zusammenführung von Expert:innenwissen sowie verschiedener Modelle zu ermöglichen und die innerbetriebliche Akzeptanz zu erhöhen, wie es \textcite{Janetzke2012} beschreiben.

Weiterhin hat sich gezeigt, dass die Modellierung der gewünschten multivariaten \ac{ms}-Prognose mittels \ac{ML} nach der \ac{drs} (\acs{vgl} \acs{kap} \ref{nutzung} \acs{bzw} \acs{abschn} \ref{lokal_schreiben}), trotz des reichhaltigen Funktionsumfangs frei verfügbarer Softwarebibliotheken, einiges an Programmieraufwand erfordert. Insbesondere das Überführen der gegebenen Zeitreihen in eine Form, welche die Anwendung verfügbarer \acs{ML} Regressionsalgorithmen erlaubt sowie eine Modellierung nach der \ac{drs} sind zum jetzigen Zeitpunkt noch nicht Teil populärer \ac{ML}-Bibliotheken wie \textit{scikit-learn}. An vielversprechenden Ergänzungen wird jedoch beispielsweise in der Bibliothek \textit{sktime} gearbeitet, die eine Implementierung einschlägiger \ac{ML}-Prognosemodelle in Zukunft aufwandsärmer gestalten könnte.

\color{black}