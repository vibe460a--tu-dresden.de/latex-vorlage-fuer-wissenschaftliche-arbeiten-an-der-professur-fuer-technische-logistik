\color{gray}
Sobald ein Modell für die gegebene Prognoseaufgabe gebildet \acs{bzw} ausgewählt ist, sollte überprüft werden, ob dieses die Zusammenhänge in den Daten tatsächlich angemessen abbilden kann. Ähnlich zu den meisten statistischen Methoden wird diese Überprüfung üblicherweise durch eine Analyse der Residuen \acs{bzw} Abweichungen

\begin{equation}
    e_t = y_t - \hat{y}_t
\end{equation}

vorgenommen, die sich aus der Differenz zwischen dem Beobachtungswert $y$ und dem Prognosewert $\hat{y}$ zu einem bestimmten Zeitpunkt $t$ ergeben. Da es sich hier um ein Zeitreihen-Modell handelt, sollten auch dessen Abweichungen als Zeitreihe betrachtet werden. \parencites[\acs{vgl}][61-62]{James2013}[59]{Vogel2015}[107]{Chatfield2019}.

Ist das Modell valide, weisen dessen Abweichungen die Charakteristik eines weißen Rauschens auf und erfüllen damit die folgenden vier Prämissen \parencites[\acs{vgl}][59]{Vogel2015}[]{Hyndman2018}[107]{Chatfield2019}:

\begin{enumerate}
    \item Die Abweichungen schwanken um einen konstanten Mittelwert nahe null.
    \item Die Varianz verhält sich konstant.
    \item Die Verteilung genügt einer Normalverteilung.
    \item Es sind keine signifikanten Autokorrelationen vorhanden \acs{bzw} die Abweichungen sind zeitlich unabhängig voneinander.
\end{enumerate}

Eine Beurteilung des Modells \acs{bzw} der Abweichungen, in Hinblick auf diese Prämissen soll hier als \textit{Validierung} bezeichnet werden. Diese wird üblicherweise durchgeführt, indem die Abweichungen durch verschiedene Plots visualisiert werden \parencites[\acs{vgl}][99]{Kuhn2013}[107]{Chatfield2019}. Ist eine der genannten Voraussetzungen nicht gegeben, enthalten die beobachteten Abweichungen Restinformationen, die zur weiteren Verbesserung des Modells dienlich sein können \parencite[\acs{vgl}][]{Hyndman2018}. Die Validierung wird hier anhand des Test-Sets (\acs{vgl} \acs{abschn} \ref{ordnerstruktur}) nachfolgend vorgenommen. Dafür werden 16 \ac{ms}-Prognosen nach der \acs{wfv} (\acs{vgl} \acs{abschn} \ref{ordnerstruktur}) generiert und mit der Saison- sowie Trendkomponente (\acs{vgl} \acs{abschn} \ref{ordnerstruktur}) beaufschlagt, wie es auch \textcite[10]{Makridakis2018} beschreiben. Diese Prognosen sind in \acs{Abb} \ref{ordnerstruktur} dargestellt. Unter Berücksichtigung der Prognosewerte aller Teil-Modelle ergeben sich anhand der Beobachtungswerte im Test-Set 48 Abweichungen, die repräsentativ für das übergeordnete \acs{ms}-Modell betrachtet werden. Weiterhin ist zu beachten, dass im Rahmen dieser Validierung nur festgestellt, jedoch nicht nachgebessert werden kann, da in diesem Zuge neue Daten gesammelt werden müssten (\acs{vgl} \acs{abschn} \ref{ordnerstruktur}), was jedoch den Rahmen dieser Arbeit überstiege.


\subsection{Normalverteilung}

    Um die Abweichungen auf Ähnlichkeit zu einer Normalverteilung zu überprüfen, wird sowohl ein qualitativer als auch ein quantitativer Test durchgeführt.
    
    Als qualitativer Test kann ein \ac{qqp} dienen, indem die empirischen Quantile der Abweichungen gegen die entsprechenden Quantile einer Normalverteilung aufgetragen werden. Sind die Abweichungen normalverteilt, liegen alle Punkte des Plots auf einer Geraden \parencite[\acs{vgl}][66-67]{Vogel2015}.
    
    Ein \ac{qqp} wird hier mittels der Klasse \verb+qqplot+ in \textit{statsmodels} erstellt und ist in \acs{Abb} \ref{fig_qq} dargestellt. Es ist ersichtlich, dass die einzelnen Punkte näherungsweise auf der abgebildeten Geraden liegen und die Abweichungen demnach einer Normalverteilung genügen.
    
    \begin{figure}[ht]
        \centering
        \includegraphics[width=\textwidth]{graphics/fig_qq.pdf}
        \caption[\acl{qqp}.]{\acl{qqp} der standardisierten, unzentrierten Abweichungen. Die schwarze Linie stellt eine ideale Normalverteilung dar.}
        \label{fig_qq}
    \end{figure}
    
    Als quantitativer Test wird die Teststatistik von \textcite{DAgostino1973} herangezogen, die in \textit{scipy} mit der Klasse \verb+normaltest+ implementiert ist. Der sich ergebende p-Wert von 0.47 übersteigt das festgelegte Signifikanzniveau $\alpha$ von 0.05, wodurch die Annahme normalverteilter Abweichungen bestätigt wird.
   
\subsection{Fit des Modells}

    Vor einer Validierung anhand der Abweichungen kann ein Plot der Beobachtungswerte gegen die Prognosewerte bei der Beurteilung der Modellanpassung (Fit) helfen \parencite[\acs{vgl}][99]{Kuhn2013}. Dieser ist in \acs{Abb} \ref{fig_obs_vs_pred} zu sehen. Der Winkelversatz zwischen der linearen Best-Fit Linie und einer theoretischen, positiven Korrelation kann als Maß für ein Defizit bezüglich des Fits herangezogen werden.
    
    \begin{figure}[ht]
        \centering
        \includegraphics[width=\textwidth]{graphics/fig_obs_vs_pred.pdf}
        \caption[Fit des Modells.]{Beobachtungswerte über die Prognosewerte. Die durchgängige schwarze Linie bildet den linearen Best-Fit und die gestrichelte stellt eine perfekte Korrelation dar.}
        \label{fig_obs_vs_pred}
    \end{figure}
